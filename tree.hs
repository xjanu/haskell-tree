
import System.Directory   (listDirectory,
                           doesDirectoryExist,
                           doesPathExist)
import System.Environment (getArgs, getProgName)
import Data.List          (elem)

version = "v1.0"
authors = "Martin Janu"

versionmsg :: String -> String
versionmsg name = name ++ " " ++ version ++ " by " ++ authors

helpmsg :: String -> String
helpmsg name = 
    ("Usage: " ++ name ++ " [-h | --help] [-v | --version] [<path>]\n" ++
     "Where\n" ++
     "  -h --help    Show this help message.\n" ++
     "  -v --version Display version information.\n" ++
     "  <path>       Root of the tree to display. Defaults to '.'")


prefix :: [Bool] -> String
prefix []         = ""
prefix (False:[]) = "'-- "
prefix (False:xs) = "    " ++ prefix xs
prefix (True:[])  = "+-- "
prefix (True:xs)  = "|   " ++ prefix xs


tree :: FilePath -> FilePath -> [Bool] -> IO ()
tree _   ('.':_) (_:_) = return ()
tree rel fp      pre =
    do ispath <- doesPathExist (rel ++ "/" ++ fp)
       isdir  <- doesDirectoryExist (rel ++ "/" ++ fp)
       if not ispath
       then putStrLn (fp ++ " [error opening dir]")
       else if isdir
       then do putStr (prefix pre)
               putStrLn fp
               children <- listDirectory (rel ++ "/" ++ fp)
               case children of ([])   -> return ()
                                (a:[]) -> tree (rel ++ "/" ++ fp) a (pre ++ [False])
                                (a:ls) -> do foldl (>>) (return ()) (map ((flip (tree (rel ++ "/" ++ fp))) (pre ++ [True])) ls)
                                             tree (rel ++ "/" ++ fp) a (pre ++ [False])
               
       else do putStr (prefix pre)
               putStrLn fp
       

main :: IO ()
main =
        -- .(".":) Prepend list with default ".", so it will never be empty
        -- if-then-else If the last argument does start with '-', then it is
        --              an option, not the path.
    do  fp <- getArgs >>= return.last.(".":) >>= (\x -> if head x == '-'
                                                        then return "."
                                                        else return x)
        args <- getArgs
        if "-h" `elem` args || "--help" `elem` args
        then getProgName >>= putStrLn.helpmsg
        else if "-v" `elem` args || "--version" `elem` args
        then getProgName >>= putStrLn.versionmsg
        else if (head fp) == '/'
        then tree "" fp []
        else tree "." fp []
