# Haskell tree

A clone of the [linux tree command](http://mama.indstate.edu/users/ice/tree/),
written entirely in Haskell.

## Todo:
(In no particular order)

- Add Unicode support
- Check folder permissions (instead of crashing)
- Add a Makefile
- Do a complete rewrite (Preferably not at midnight this time)

## Bugs:

- Sometimes doesn't work.
- The code is unmaintainable.
